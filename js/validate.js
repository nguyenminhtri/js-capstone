function checkBlank(id, alertID) {
  var text = document.getElementById(id).value;

  if (text == "") {
    document.getElementById(alertID).innerHTML = "(Không được để trống)";
    return false;
  } else {
    document.getElementById(alertID).innerHTML = "";
    return true;
  }
}

function checkDup(id, alertID, arr) {
  var text = document.getElementById(id).value;

  for (let i = 0; i < arr.length; i++) {
    if (text == arr[i].name) {
      console.log("yes");
      document.getElementById(alertID).innerHTML = "(Đã tồn tại)";
      return false;
    } else {
      document.getElementById(alertID).innerHTML = "";
      return true;
    }
  }
}
