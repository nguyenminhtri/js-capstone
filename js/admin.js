const DATA_URL = "https://63676a8b79b0914b75dff07d.mockapi.io/product";
let isValid = true;
let localData = [];

// Validation
function checkValid(mode) {
  if (mode == "checkDup") {
    isValid =
      checkBlank("productName", "productNameAlert") &&
      checkDup("productName", "productNameAlert", localData);
  } else if (mode == "uncheckDup") {
    isValid = checkBlank("productName", "productNameAlert");
  }
  isValid =
    isValid &
    (checkBlank("productBackCam", "productSpecAlert") &&
      checkBlank("productFrontCam", "productSpecAlert") &&
      checkBlank("productScreen", "productSpecAlert"));

  isValid =
    isValid &
    checkBlank("productPrice", "productPriceAlert") &
    checkBlank("productDesc", "productDescAlert") &
    checkBlank("productImage", "productImageAlert");
  console.log("isValid: ", isValid);
}

// Lấy dữ liệu từ database
function getData() {
  loading("on");
  axios({
    url: DATA_URL,
    method: "GET",
  })
    .then(function (res) {
      localData = res.data;
      renderData(res.data);
      loading("off");
    })
    .catch(function (err) {
      console.log("err: ", err);
      loading("off");
    });
}

// Thêm item
function addItem() {
  checkValid("checkDup");
  if (!isValid) return;

  var product = getFormInfo();
  axios({
    url: DATA_URL,
    method: "POST",
    data: product,
  })
    .then(function (res) {
      getData();
      loading("off");
    })
    .catch(function (err) {
      console.log("err: ", err);
      loading("off");
    });
}

// Xóa item
function deleteItem(index) {
  loading("on");
  axios({
    url: `${DATA_URL}/${index}`,
    method: "DELETE",
  })
    .then(function (res) {
      getData();
      loading("off");
    })
    .catch(function (err) {
      console.log("err: ", err);
      loading("off");
    });
}

// Chỉnh sửa item
let updateID = -1;
function editItem(dataID) {
  loading("on");
  toggleBtn(1);
  axios({
    url: `${DATA_URL}/${dataID}`,
    method: "GET",
  })
    .then(function (res) {
      fillFormInfo(res.data);
      updateID = res.data.id;
      loading("off");
    })
    .catch(function (err) {
      console.log("err: ", err);
      loading("off");
    });
}

// Update item lên database
function updateItem() {
  checkValid("uncheckDup");
  if (!isValid) return;

  loading("on");
  var newData = getFormInfo();
  axios({
    url: `${DATA_URL}/${updateID}`,
    method: "PUT",
    data: newData,
  })
    .then(function (res) {
      getData();
      loading("off");
    })
    .catch(function (err) {
      console.log("err: ", err);
      loading("off");
    });
}
