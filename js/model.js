function CartItem(itemID, itemName, itemPrice, itemQty, img) {
  this.id = itemID;
  this.name = itemName;
  this.price = itemPrice;
  this.qty = itemQty;
  this.img = img;
  this.checkout = () => {
    return this.price * this.qty;
  };
}
