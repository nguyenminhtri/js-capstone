const DATA_URL = "https://63676a8b79b0914b75dff07d.mockapi.io/product";

const CART_JSON = "cart-item";

// Mảng chứa dữ liệu lấy từ database
let productList = [];
// Mảng chứa dữ liệu giỏ hàng
let cartItemList = [];

// Lấy dữ liệu từ database
function getData() {
  axios({
    url: DATA_URL,
    method: "GET",
  })
    .then(function (res) {
      productList = res.data;
      renderProductList(productList);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

getData();
loadLocalStorage();

// Lưu vào local storage
function saveLocalStorage() {
  var toJSON = JSON.stringify(cartItemList);
  localStorage.setItem(CART_JSON, toJSON);
}

//Load data khi tải trang
function loadLocalStorage() {
  var dataJSON = localStorage.getItem(CART_JSON);
  if (dataJSON !== null) {
    var tempArr = JSON.parse(dataJSON);

    // Convert Data
    tempArr.forEach((item) => {
      var oldItem = new CartItem(
        item.id,
        item.name,
        item.price,
        item.qty,
        item.img
      );
      cartItemList.push(oldItem);
    });
    renderCartList();
    saveLocalStorage();
  }
}
// document.getElementById("chonDt").onchange = function () {
//   let searchItem = [];
//   var loaiDt = document.getElementById("chonDt").value;
//   if (loaiDt === "samsung") {
//     productList.forEach((item) => {
//       if (item.type == "Samsung") {
//         searchItem.push(item);
//         renderProductList(searchItem);
//       }
//     });
//   } else if (loaiDt === "iphone") {
//     productList.forEach((item) => {
//       if (item.type == "iphone") {
//         searchItem.push(item);
//         renderProductList(searchItem);
//       }
//     });
//   } else {
//     renderProductList(productList);
//   }
// };

function searchPhone() {
  // Mảng chứa dữ liệu tìm kiếm
  let searchItem = [];

  var keyWord = document.getElementById("timDt").value;
  var regexIphone = /iphone|apple/giu;
  var regexSS = /samsu|galaxy/giu;

  if (regexIphone.test(keyWord)) {
    productList.forEach((item) => {
      if (item.type == "iphone") {
        searchItem.push(item);
      }
    });
  } else if (regexSS.test(keyWord)) {
    productList.forEach((item) => {
      if (item.type == "Samsung") {
        searchItem.push(item);
      }
    });
  } else
    return (document.getElementById(
      "danhSachDt"
    ).innerHTML = `<p>Không tìm thấy sản phẩm "${keyWord}"</p>`);

  renderProductList(searchItem);
}

// Thêm chức năng enter cho nút search
document.getElementById("timDt").addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    document.getElementById("btnTimDt").click();
  }
});

// Lọc phone theo type
function filterPhone() {
  let filteredItem = [];
  var filteredWord = document.getElementById("selectPhoneType").value;

  if (filteredWord == 0) {
    renderProductList(productList);
    return;
  } else {
    productList.forEach((item) => {
      if (filteredWord === item.type) {
        filteredItem.push(item);
      }
    });
    renderProductList(filteredItem);
    return;
  }
}

// Thêm item vào giỏ hàng
function addToCard(itemID) {
  var itemName = productList[itemID].name;
  var itemPrice = productList[itemID].price;
  var itemImg = productList[itemID].img;
  var item = new CartItem(itemID, itemName, itemPrice, 1, itemImg);
  if (cartItemList.length == 0) {
    cartItemList.push(item);
    renderCartList();
    saveLocalStorage();
    return item;
  }

  for (var i = 0; i < cartItemList.length; i++) {
    if (cartItemList[i].id == itemID) {
      cartItemList[i].qty++;
      break;
    } else if (i == cartItemList.length - 1) {
      cartItemList.push(item);
      break;
    }
  }

  renderCartList();
  saveLocalStorage();
}

// Nút tăng số lượng
function decreaseItem(itemIndex) {
  cartItemList[itemIndex].qty--;
  if (cartItemList[itemIndex].qty == 0) {
    cartItemList.splice(itemIndex, 1);
  }
  renderCartList();
  saveLocalStorage();
}

// Nút giảm số lượng
function increaseItem(itemIndex) {
  cartItemList[itemIndex].qty++;
  renderCartList();
  saveLocalStorage();
}

// Xóa item
function deleteItem(itemIndex) {
  cartItemList.splice(itemIndex, 1);
  renderCartList();
  saveLocalStorage();
}

function checkOut() {
  if (cartItemList.length >= 1) {
    cartItemList = [];
    alert("Cảm ơn bạn đã ủng hộ");
    renderCartList();
    saveLocalStorage();
  }
}
