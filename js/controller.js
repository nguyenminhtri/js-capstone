// Hiển thị thông tin lên web
function renderProductList(arr) {
  var listContent = "";

  arr.forEach((item, index) => {
    // Giao diện HTML của item ở trong đây
    // item start
    listContent += `
    <div class="item">
  <div class="item__img">
    <img src="${item.img}" />
    <div class="overlay">
      <div class="logo">
        <img width= "50px" src="./image/${
          item.type == "iphone" ? "apple_logo.svg" : "samsung_wordmark.svg"
        }">
      </div>
      <div class="detail">
        <h5>${item.desc}</h5>
        <p>
          <span>${item.screen}</span><br />
          <span>${item.backCamera}</span><br />
          <span>${item.frontCamera}</span><br />
        </p>
      </div>
    </div>
  </div>

  <div class="item__bottom">
    <h4>${item.price}đ</h4>
    <h3>${item.name}</h3>
    <button onclick="addToCard(${index})">Add to Cart</button>
  </div>
</div>
      `;
    // item end
  });

  document.getElementById("danhSachDt").innerHTML = listContent;
}

// Hiển thị thông tin giỏ hàng
function renderCartList() {
  var listContent = "";
  var total = 0;
  var tongSo = 0;
  cartItemList.forEach((item, index) => {
    listContent += `
        <tr>
            <td>${item.name}</td>
            <td>${item.price}</td>
            <td><i class="fa fa-angle-left mr-2" onclick="decreaseItem(${index})"
            
            ></i> ${
              item.qty
            } <i class="fa fa-angle-right ml-2" onclick="increaseItem(${index})"></i></td>
           
            <td>${item.checkout()}</td>
            <td><img style="width:50px;height:50px" src="${item.img}" /></td>
            <td><button class="btn btn-danger" onclick="deleteItem(${index})">X</button></td>
        </tr>
        `;
    // Tính tổng tiền
    total += item.checkout();
    tongSo = item.qty + tongSo;
  });

  document.getElementById("cartTbody").innerHTML = listContent;
  document.getElementById("totalCheckOut").innerHTML = total;
  document.getElementById("so").innerHTML = tongSo;
}
// function openMenu() {
//   document.getElementById("table").classList.toggle("disPlay");
// }
