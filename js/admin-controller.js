// Render data lên trang admin
function renderData(arr) {
  let contentHTML = "";

  arr.forEach((item) => {
    contentHTML += `
      <tr>
      <td>${item.id}</td>
      <td>${item.name}</td>
      <td>${item.price}</td>
      <td>${item.screen}</br>Cam trước: ${item.backCamera}</br>Cam sau: ${item.frontCamera}</td>
      <td><img src="${item.img}" /></td>
      <td>${item.desc}</td>
      <td>${item.type}</td>
      <td class="text-center">
        <button data-bs-toggle="modal"
        data-bs-target="#productInfo" class="btn btn-secondary py-1 px-2 my-1" onclick="clearErr();editItem(${item.id})">
          <i class="material-icons">edit</i>
        </button>
        <button class="btn btn-danger py-1 px-2 my-1" onclick="deleteItem(${item.id})">
          <i class="material-icons">delete_forever</i>
        </button>
      </td>
    </tr>
      `;
  });
  document.getElementById("productList").innerHTML = contentHTML;
}

// Lấy thông tin từ form
function getFormInfo() {
  var name = document.getElementById("productName").value;
  var price = document.getElementById("productPrice").value;
  var screen = document.getElementById("productScreen").value;
  var frontCamera = document.getElementById("productFrontCam").value;
  var backCamera = document.getElementById("productBackCam").value;
  var img = document.getElementById("productImage").value;
  var desc = document.getElementById("productDesc").value;
  var type = document.getElementById("productType").value;

  return { name, price, screen, backCamera, frontCamera, img, desc, type };
}

// Chuyển thông tin lên form
function fillFormInfo(data) {
  document.getElementById("productName").value = data.name;
  document.getElementById("productPrice").value = data.price;
  document.getElementById("productScreen").value = data.screen;
  document.getElementById("productFrontCam").value = data.frontCamera;
  document.getElementById("productBackCam").value = data.backCamera;
  document.getElementById("productImage").value = data.img;
  document.getElementById("productDesc").value = data.desc;
  document.getElementById("productType").value = data.type;
  document.getElementById("imagePreview").src = data.img;
}

// Hình tự cập nhật khi dán link
function changeImage() {
  var img = document.getElementById("productImage").value;

  document.getElementById("imagePreview").src = img;
}

// Hoán đổi nút Add và Update
function toggleBtn(mode) {
  var linkAdd = document.getElementById("addBtn");
  var linkUpdate = document.getElementById("updateBtn");
  // 0: Add, 1: Update
  if (mode == 0) {
    linkAdd.style.display = "inline";
    linkUpdate.style.display = "none";
  } else if (mode == 1) {
    linkAdd.style.display = "none";
    linkUpdate.style.display = "inline";
  }
}

function loading(mode) {
  switch (mode) {
    case "on":
      document.getElementById("loading").style.display = "flex";
      break;
    case "off":
      document.getElementById("loading").style.display = "none";
      break;
  }
}

function clearForm() {
  document.querySelector("#productInfo form").reset();
  document.getElementById("imagePreview").src =
    "https://cdn-icons-png.flaticon.com/512/0/191.png";
}
function clearErr() {
  document.getElementById("productNameAlert").innerHTML = "";
  document.getElementById("productPriceAlert").innerHTML = "";
  document.getElementById("productSpecAlert").innerHTML = "";
  document.getElementById("productDescAlert").innerHTML = "";
  document.getElementById("productImageAlert").innerHTML = "";
}
